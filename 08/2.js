const {
  readInput,
  arrayIntersect
} = require('../helpers')
const path = require('path')

const sortStr = (str) => {
  return str.split('').sort().join('')
}

const input = readInput(path.join(__dirname, 'input.txt')).split('\n').map(x => {
  const [pattern, output] = x.split(' | ')
  return {
    pattern: pattern.trim().split(' '),
    output: output.trim().split(' ')
  }
})

let sum = 0
input.forEach(item => {
  let digits = item.pattern.reduce((acc, x) => {
    if (x.length === 2) {
      acc[1] = sortStr(x)
    } else if (x.length === 3) {
      acc[7] = sortStr(x)
    } else if (x.length === 4) {
      acc[4] = sortStr(x)
    } else if (x.length === 7) {
      acc[8] = sortStr(x)
    }

    return acc
  }, {
    0: null,
    1: null,
    2: null,
    3: null,
    4: null,
    5: null,
    6: null,
    7: null,
    8: null,
    9: null
  })

  digits = item.pattern.reduce((acc, x) => {
    if (x.length === 6) {
      if (
        arrayIntersect(x.split(''), acc[4].split('')).length !== 4 &&
        arrayIntersect(x.split(''), acc[1].split('')).length === 2
      ) {
        acc[0] = sortStr(x)
      } else if (
        arrayIntersect(x.split(''), acc[4].split('')).length !== 4 &&
        arrayIntersect(x.split(''), acc[1].split('')).length !== 2
      ) {
        acc[6] = sortStr(x)
      } else {
        acc[9] = sortStr(x)
      }
    } else if (x.length === 5) {
      if (
        arrayIntersect(x.split(''), acc[4].split('')).length === 3 &&
        arrayIntersect(x.split(''), acc[7].split('')).length === 3
      ) {
        acc[3] = sortStr(x)
      } else if (
        arrayIntersect(x.split(''), acc[4].split('')).length === 3 &&
        arrayIntersect(x.split(''), acc[7].split('')).length === 2
      ) {
        acc[5] = sortStr(x)
      } else {
        acc[2] = sortStr(x)
      }
    }

    return acc
  }, digits)

  const digitArray = Object.values(digits)

  const num = parseInt(item.output.map(x => digitArray.indexOf(sortStr(x))).join(''), 10)
  sum += num
})

console.log('Result', sum)
