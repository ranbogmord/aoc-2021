const { readInput } = require('../helpers')
const path = require('path')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n').map(x => {
  const [pattern, output] = x.split(' | ')
  return {
    pattern: pattern.trim().split(' '),
    output: output.trim().split(' ')
  }
})

console.log('Result', input.reduce((acc, row) => {
  return acc + row.output.filter(x => [2, 3, 4, 7].indexOf(x.length) !== -1).length
}, 0))
