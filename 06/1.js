const path = require('path')
const { readInput } = require('../helpers')

let input = readInput(path.join(__dirname, 'input.txt')).split(',').map(x => parseInt(x, 10))

for (let i = 0; i < 80; i++) {
  let tmpInput = [...input]
  tmpInput = tmpInput.map(x => x - 1)
  const newFish = []
  tmpInput = tmpInput.map(x => {
    if (x < 0) {
      newFish.push(8)
      return 6
    } else {
      return x
    }
  })

  if (newFish.length > 0) {
    tmpInput = tmpInput.concat(newFish)
  }

  input = [...tmpInput]
}

console.log('Result', input.length)
