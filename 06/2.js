const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, 'input.txt')).split(',').map(x => parseInt(x, 10))

let state = [0, 0, 0, 0, 0, 0, 0, 0, 0]

input.forEach(item => {
  state[item] += 1
})

for (let i = 0; i < 256; i++) {
  const newState = [...state]
  const zeroes = newState.shift()

  newState[8] = zeroes
  newState[6] += zeroes
  state = newState
}

console.log('Result', state.reduce((acc, item) => acc + item, 0))
