const { readInput } = require('../helpers')
const path = require('path')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n')

const extractCoords = (coords) => {
  const [x, y] = coords.split(',')
  return { x: parseInt(x, 10), y: parseInt(y, 10) }
}

const parseRow = (row) => {
  const [from, to] = row.trim().split(' -> ')
  return {
    from: extractCoords(from),
    to: extractCoords(to)
  }
}

const filterEqualCoords = (row) => row.from.x === row.to.x || row.from.y === row.to.y

const lineToPoints = line => {
  const from = { ...line.from }
  const to = { ...line.to }
  const points = []

  while (from.x !== to.x || from.y !== to.y) {
    points.push({ ...from })
    if (from.x !== to.x) {
      if (from.x > to.x) {
        from.x -= 1
      } else {
        from.x += 1
      }
    } else if (from.y !== to.y) {
      if (from.y > to.y) {
        from.y -= 1
      } else {
        from.y += 1
      }
    }
  }
  points.push({ ...from })

  return points
}

const mapped = input
  .map(parseRow)
  .filter(filterEqualCoords)
  .map(lineToPoints)
  .reduce((acc, line) => {
    return acc.concat(line)
  }, [])
  .reduce((acc, point) => {
    const key = `${point.x},${point.y}`
    if (!(key in acc)) {
      acc[key] = 0
    }
    acc[key]++
    return acc
  }, {})

console.log('Result', Object.keys(mapped).filter(x => mapped[x] > 1).length)
