const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n')

const applyCommand = (state, cmd) => {
  const [instr, amount] = cmd.split(' ')

  if (instr === 'forward') {
    state.horiz += parseInt(amount, 10)
  } else if (instr === 'down') {
    state.depth += parseInt(amount, 10)
  } else if (instr === 'up') {
    state.depth -= parseInt(amount, 10)
  }

  return state
}

const result = input.reduce(applyCommand, { horiz: 0, depth: 0 })
console.log(result)
console.log('Result', result.horiz * result.depth)
