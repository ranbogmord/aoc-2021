const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n')

const applyCommand = (state, cmd) => {
  const [instr, amount] = cmd.split(' ')
  const amountInt = parseInt(amount, 10)

  if (instr === 'forward') {
    state.horiz += amountInt
    state.depth += state.aim * amountInt
  } else if (instr === 'down') {
    state.aim += amountInt
  } else if (instr === 'up') {
    state.aim -= amountInt
  }

  return state
}

const result = input.reduce(applyCommand, { horiz: 0, depth: 0, aim: 0 })
console.log(result)
console.log('Result', result.horiz * result.depth)
