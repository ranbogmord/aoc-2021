const fs = require('fs')

const helpers = {
  readInput: (path) => {
    return fs.readFileSync(path).toString()
  },
  nullSum: (items) => {
    return items.filter(x => x).reduce((acc, item) => {
      acc += item
      return acc
    }, 0)
  },
  arrayIntersect: (arr1, arr2) => {
    return arr1.filter(x => arr2.includes(x))
  },
  readIntegerArrayInput: (path, separator = '') => {
    return fs.readFileSync(path).toString().split(separator).map(x => parseInt(x.trim()))
  },
  readIntegerMatrixInput: (path, separator = '', lineSeparator = '\n') => {
    return fs.readFileSync(path)
      .toString()
      .split(lineSeparator)
      .map(x => {
        return x.trim().split(separator).map(x => parseInt(x.trim()))
      })
  },
  inBounds: (coord, matrix, row) => {
    if (coord[0] >= 0 && coord[0] < matrix.length) {
      if (coord[1] >= 0 && coord[1] < matrix[row].length) {
        return true
      }
    }

    return false
  }
}

module.exports = helpers
