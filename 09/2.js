const { readInput } = require('../helpers')
const path = require('path')

const input = readInput(path.join(__dirname, 'input.txt'))
  .split('\n')
  .reduce((acc, item) => {
    acc.push(item.trim().split('').map(x => parseInt(x)))
    return acc
  }, [])

const lowPoints = []
for (let i = 0; i < input.length; i++) {
  for (let j = 0; j < input[i].length; j++) {
    const up = i - 1
    const right = j + 1
    const bottom = i + 1
    const left = j - 1

    const current = input[i][j]

    if (
      (up < 0 || input[up][j] > current) &&
      (right >= input[i].length || input[i][right] > current) &&
      (bottom >= input.length || input[bottom][j] > current) &&
      (left < 0 || input[i][left] > current)
    ) {
      lowPoints.push([i, j])
    }
  }
}

const checkPoint = (point, visited = []) => {
  if (visited.indexOf(`${point[0]},${point[1]}`) !== -1) {
    return 0
  } else if (point[0] < 0 || point[0] >= input.length || point[1] < 0 || point[1] >= input[point[0]].length) {
    return 0
  } else if (input[point[0]][point[1]] === 9) {
    return 0
  } else {
    visited.push(`${point[0]},${point[1]}`)
    return checkPoint([point[0] - 1, point[1]], visited) + // up
      checkPoint([point[0], point[1] + 1], visited) + // right
      checkPoint([point[0] + 1, point[1]], visited) + // bottom
      checkPoint([point[0], point[1] - 1], visited) + // left
      1 // this point
  }
}

console.log('Result', lowPoints.map(x => checkPoint(x)).sort((a, b) => b - a).slice(0, 3).reduce((tot, x) => tot * x, 1))
