const { readInput } = require('../helpers')
const path = require('path')

const input = readInput(path.join(__dirname, 'input.txt'))
  .split('\n')
  .reduce((acc, item) => {
    acc.push(item.trim().split('').map(x => parseInt(x)))
    return acc
  }, [])

let riskSum = 0
for (let i = 0; i < input.length; i++) {
  for (let j = 0; j < input[i].length; j++) {
    const up = i - 1
    const right = j + 1
    const bottom = i + 1
    const left = j - 1

    const current = input[i][j]

    if (
      (up < 0 || input[up][j] > current) &&
      (right >= input[i].length || input[i][right] > current) &&
      (bottom >= input.length || input[bottom][j] > current) &&
      (left < 0 || input[i][left] > current)
    ) {
      riskSum += (1 + current)
    }
  }
}

console.log('Result', riskSum)
