const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, 'input.txt')).split(',').map(x => parseInt(x, 10))

const adjustPositions = (items, target) => {
  return items.reduce((acc, item) => {
    const diff = Math.abs(Math.max(item, target) - Math.min(item, target))
    for (let i = 0; i <= diff; i++) {
      acc += i
    }
    return acc
  }, 0)
}

const max = input.reduce((acc, item) => item > acc ? item : acc, Number.MIN_VALUE)
const min = input.reduce((acc, item) => item < acc ? item : acc, Number.MAX_VALUE)

let minCost = Number.MAX_VALUE
for (let i = min; i <= max; i++) {
  const cost = adjustPositions(input, i)
  if (cost < minCost) {
    minCost = cost
  }
}

console.log('Result', minCost)
