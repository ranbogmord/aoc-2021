const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, '/input.txt')).split('\n').map(x => parseInt(x, 10))

let last = null
let increased = 0
for (let i = 0; i < input.length - 2; i++) {
  const sum = input[i] + input[i + 1] + input[i + 2]
  if (last === null) {
    last = sum
    continue
  }

  if (sum > last) {
    increased += 1
  }
  last = sum
}

console.log('Result', increased)
