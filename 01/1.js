const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, '/input.txt'))

const result = input.split('\n').reduce((acc, depth) => {
  depth = parseInt(depth, 10)
  if (acc.lastValue === null) {
    acc.lastValue = depth
    return acc
  }

  if (depth > acc.lastValue) {
    acc.increased += 1
  }

  acc.lastValue = depth
  return acc
}, {
  increased: 0,
  lastValue: null
})

console.log('Result', result.increased)
