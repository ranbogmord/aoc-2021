const { readInput, arrayIntersect } = require('../helpers')
const path = require('path')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n')

let numbers = null
const boards = []

let currentBoard = null
for (let i = 0; i < input.length; i++) {
  if (i === 0) {
    numbers = input[i].trim().split(',')
    continue
  }

  if (!input[i].trim()) {
    if (currentBoard) {
      boards.push(currentBoard)
    }
    currentBoard = null
    continue
  }

  if (!currentBoard) {
    currentBoard = {
      rows: [],
      columns: []
    }
  }

  const row = input[i].trim().replace(/\s{2,}/g, ' ').split(' ')
  currentBoard.rows.push(row)

  row.forEach((item, col) => {
    if (!currentBoard.columns[col]) {
      currentBoard.columns[col] = []
    }

    if (item) {
      currentBoard.columns[col][i] = item
      currentBoard.columns[col] = currentBoard.columns[col].filter(x => x)
    }
  })
}

const nums = numbers.slice(0, 4)
let winner = null

for (let i = 0; i < numbers.length; i++) {
  if (winner) break
  nums.push(numbers[i])

  for (let j = 0; j < boards.length; j++) {
    const board = boards[j]

    let winners = board.rows.filter(row => {
      return row.length === arrayIntersect(row, nums).length
    })

    if (winners.length > 0) {
      winner = board.rows
      break
    }

    winners = winners.concat(board.columns.filter(col => {
      return col.length === arrayIntersect(col, nums).length
    }))

    if (winners.length > 0) {
      winner = board.columns
      break
    }
  }
}

const result = winner.reduce((acc, item) => {
  return acc.concat(item)
}, [])
  .filter(x => !nums.includes(x))
  .reduce((acc, item) => acc + parseInt(item, 10), 0)

console.log('Result', result * parseInt(nums[nums.length - 1], 10))
