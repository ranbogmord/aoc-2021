const {
  readIntegerMatrixInput,
  inBounds
} = require('../helpers')
const path = require('path')

let input = readIntegerMatrixInput(path.join(__dirname, 'input.txt'))

const width = input[0].length
const height = input.length
let totalFlashes = 0
let fullFlashStep = -1

function flashCell (i, j) {
  totalFlashes += 1
  input[i][j] = -1
  const diffs = [-1, 0, 1]

  diffs.forEach(r => {
    diffs.forEach(c => {
      const dx = i + r
      const dy = j + c

      if (inBounds([dx, dy], input, j) && input[dx][dy] !== -1) {
        input[dx][dy] += 1
        if (input[dx][dy] >= 10) {
          flashCell(dx, dy)
        }
      }
    })
  })
}

let step = 1
while (fullFlashStep === -1) {
  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      input[i][j] += 1
    }
  }

  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      if (input[i][j] === 10) {
        flashCell(i, j)
      }
    }
  }

  input = input.map(row => row.map(col => col === -1 ? 0 : col))
  if (totalFlashes === height * width) {
    fullFlashStep = step
    break
  }
  totalFlashes = 0
  step += 1
}

console.log('Result', totalFlashes, 'on step', fullFlashStep)
