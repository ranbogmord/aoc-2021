const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n')

const findValues = (rows, isOxygen, col = 0) => {
  if (rows.length === 1) {
    return rows.pop()
  }

  const parsed = rows.reduce((acc, row) => {
    const cols = row.split('')
    if (cols[col] === '1') {
      acc.ones.count += 1
      acc.ones.rows.push(row)
    } else {
      acc.zeroes.count += 1
      acc.zeroes.rows.push(row)
    }

    return acc
  }, {
    ones: { count: 0, rows: [] },
    zeroes: { count: 0, rows: [] }
  })

  if (isOxygen) {
    if (parsed.ones.count >= parsed.zeroes.count) {
      return findValues(parsed.ones.rows, isOxygen, col + 1)
    } else {
      return findValues(parsed.zeroes.rows, isOxygen, col + 1)
    }
  } else {
    if (parsed.zeroes.count <= parsed.ones.count) {
      return findValues(parsed.zeroes.rows, isOxygen, col + 1)
    } else {
      return findValues(parsed.ones.rows, isOxygen, col + 1)
    }
  }
}

const oxygen = findValues(input, true, 0)
const co2 = findValues(input, false, 0)

console.log('Result', parseInt(oxygen, 2) * parseInt(co2, 2))
