const path = require('path')
const { readInput } = require('../helpers')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n')

const result = input.reduce((acc, row) => {
  row.split('').forEach((digit, idx) => {
    if (!acc[idx]) {
      acc[idx] = { ones: 0, zeroes: 0 }
    }

    if (digit === '1') {
      acc[idx].ones += 1
    } else {
      acc[idx].zeroes += 1
    }
  })

  return acc
}, [])

const result2 = result.reduce((acc, row) => {
  if (row.ones > row.zeroes) {
    acc.gamma.push(1)
    acc.epsilon.push(0)
  } else {
    acc.gamma.push(0)
    acc.epsilon.push(1)
  }
  return acc
}, { gamma: [], epsilon: [] })

const gamma = parseInt(result2.gamma.join(''), 2)
const epsilon = parseInt(result2.epsilon.join(''), 2)
console.log('Result', gamma * epsilon)
