const { readInput } = require('../helpers')
const path = require('path')

let input = readInput(path.join(__dirname, 'input.txt')).split('\n').map(x => x.trim())

const chunks = {
  '(': ')',
  '[': ']',
  '{': '}',
  '<': '>'
}

const scores = {
  ')': 1,
  ']': 2,
  '}': 3,
  '>': 4
}

const isRowValid = (row) => {
  const openers = []
  let totalScore = 0

  row.split('').forEach(char => {
    if (char in chunks) {
      openers.push(char)
    } else {
      if (char !== chunks[openers.pop()]) {
        totalScore += scores[char]
      }
    }
  })

  return totalScore === 0
}

input = input.filter(x => isRowValid(x))
const inputScores = input.map(row => {
  const openers = []
  row.split('').forEach(char => {
    if (char in chunks) {
      openers.push(char)
    } else {
      openers.pop()
    }
  })

  const closers = openers.reverse().map(char => chunks[char])
  return closers.reduce((acc, char) => {
    acc = (acc * 5) + scores[char]
    return acc
  }, 0)
})

const sortedScores = inputScores.sort((a, b) => a - b)
console.log('Result', sortedScores[Math.ceil(sortedScores.length / 2) - 1])
