const { readInput } = require('../helpers')
const path = require('path')

const input = readInput(path.join(__dirname, 'input.txt')).split('\n').map(x => x.trim())

const chunks = {
  '(': ')',
  '[': ']',
  '{': '}',
  '<': '>'
}

const scores = {
  ')': 3,
  ']': 57,
  '}': 1197,
  '>': 25137
}

let totalScore = 0
input.forEach(row => {
  const openers = []

  row.split('').forEach(char => {
    if (char in chunks) {
      openers.push(char)
    } else {
      if (char !== chunks[openers.pop()]) {
        totalScore += scores[char]
      }
    }
  })
})

console.log('Result', totalScore)
